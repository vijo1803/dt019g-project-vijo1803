/**
    @file GlobalConstants.h
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Header file containing global constants
*/


#ifndef DT019G_PROJECT_VIJO1803_GLOBALCONSTANTS_H
#define DT019G_PROJECT_VIJO1803_GLOBALCONSTANTS_H

const char DELIM = '|';
const std::string FILE_NAME = "jukebox.txt";
const std::string ALBUM_SYMBOL = "***";
const std::string SECTION_DELIM = "================================";
const int MAX_NO_OF_TRACKS_ON_ALBUM = 100;
const int QUEUE_INCREMENT = 5;

#endif //DT019G_PROJECT_VIJO1803_GLOBALCONSTANTS_H
