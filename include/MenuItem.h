/**
    @file MenuItem.h
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Definition file for the menu item class
*/

#ifndef DT019G_PROJECT_VIJO1803_MENUITEM_H
#define DT019G_PROJECT_VIJO1803_MENUITEM_H

#include <string>

class MenuItem
{
private:
    std::string menuText;
    bool enabled = false;

public:
    MenuItem();
    MenuItem(std::string mt, bool en);

    // Lämpliga setters och getters deklareras här
    bool isEnabled() const;
    void enable();
    void disable();
    std::string getMenuText() const;
};

#endif //DT019G_PROJECT_VIJO1803_MENUITEM_H