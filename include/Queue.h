/**
    @file Queue.h
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Definition file for the jukebox queue
*/


#ifndef DT019G_PROJECT_VIJO1803_QUEUE_H
#define DT019G_PROJECT_VIJO1803_QUEUE_H

#include "Song.h"

class Queue
{
private:
    Song *pointer; //Pekar på aktuell sång i arrayen
    size_t size; //Storlek på arrayen
    size_t firstInQueue, lastInQueue; //Första och sista plats i kön

public:
    // Konstruktorer och destruktor
    Queue();
    ~Queue();

    // Funktioner
    void enQueue(const Song &song);
    Song deQueue();
    void increment();
    void play();

    // Operatoröverlagring
    Queue& operator=(Queue const &Q);
};

#endif //DT019G_PROJECT_VIJO1803_QUEUE_H
