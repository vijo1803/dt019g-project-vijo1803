/**
    @file Time.h
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Definition file for the custom time class
*/

#ifndef DT019G_PROJECT_VIJO1803_TIME_H
#define DT019G_PROJECT_VIJO1803_TIME_H

#include <iostream>

class Time
{
private:
    int timmar;
    int minuter;
    int sekunder;

public:
    // Konstruktorer
    Time();
    Time(int h, int m, int s);
    explicit Time(int s);

    // Getters och setters
    int getTimmar() const;
    int getMinuter() const;
    int getSekunder() const;
    void setTimmar(int t);
    void setMinuter(int m);
    void setSekunder(int s);

    // Övr
    int toIntTime() const;
    void printElegant() const;

    // Operatoröverlagringar
    Time operator+(const Time &time) const;
    bool operator==(const Time &time) const;
    bool operator<(const Time &time) const;
};

std::ostream &operator<<(std::ostream &os, const Time &time);
std::istream &operator>>(std::istream &is, Time &time);

#endif //DT019G_PROJECT_VIJO1803_TIME_H