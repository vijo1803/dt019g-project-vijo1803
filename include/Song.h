/**
    @file Song.h
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Definition file for the song class
*/


#ifndef DT019G_PROJECT_VIJO1803_SONG_H
#define DT019G_PROJECT_VIJO1803_SONG_H

#include <string>
#include "Time.h"

class Song
{
private:
    std::string title;
    std::string artist;
    Time length;

public:
    // Konstruktorer
    Song();
    //Song(std::string &nTitle, std::string &nArtist, Time &nLength);

    // Getters och setters
    std::string getTitle() const;
    std::string getArtist() const;
    Time getLength() const;
    void setTitle(const std::string &nTitle);
    void setArtist(const std::string &nArtist);
    void setLength(const Time &nLength);

    // Print
    void printSong() const;
};

std::ostream &operator<<(std::ostream &os, const Song &song);
std::istream &operator>>(std::istream &is, Song &song);

#endif
