/**
    @file Album.h
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Definition file for the album class
*/


#ifndef DT019G_PROJECT_VIJO1803_ALBUM_H
#define DT019G_PROJECT_VIJO1803_ALBUM_H

#include <string>
#include <vector>
#include "Song.h"

class Album
{
private:
    std::string name;
    std::vector<Song> songs;

public:
    Album();
    void addSong(Song &s);

    // Getters och setters
    std::string getName() const;
    std::vector<Song> getSongs() const;
    void setName(const std::string &n);

    // Utskrift
    void printAlbum() const;

    // Operatoröverlagring (mindre än)
    bool operator<(const Album &album) const;
};

// ICKE-MEDLEMMAR NEDAN
std::ostream &operator<<(std::ostream &os, const Album &album);
std::istream &operator>>(std::istream &is, Album &album);

#endif //DT019G_PROJECT_VIJO1803_ALBUM_H