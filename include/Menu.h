/**
    @file Menu.h
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Definition file for the menu class
*/


#ifndef DT019G_PROJECT_VIJO1803_MENU_H
#define DT019G_PROJECT_VIJO1803_MENU_H

#include <string>
#include <vector>
#include "MenuItem.h"

using std::string;

class Menu
{
private:
    std::vector<MenuItem> items;
    std::string menuTitle;

public:
    Menu();
    void addItem(std::string menuText, bool enabled);
    void printMenuItems() const;
    int getMenuChoice() const;
    void setMenuTitle(string t);
    void setAllEnabled();
};


#endif //DT019G_PROJECT_VIJO1803_MENU_H
