/**
    @file Jukebox.h
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Definition file for the jukebox class (user interface)
*/


#ifndef DT019G_PROJECT_VIJO1803_JUKEBOX_H
#define DT019G_PROJECT_VIJO1803_JUKEBOX_H

#include "Album.h"
#include "Menu.h"
#include "Queue.h"
#include <vector>
#include <string>

class Jukebox
{
private:
    // Datamedlemmar, albumvektor och kö
    std::vector<Album> albums;
    Queue queue;

    // Datamedlemmar, menyer
    Menu menuMain;
    Menu menuFile;
    Menu menuPrint;
    Menu menuPlay;

    // Initiera menyer
    void initMenuMain();
    void initMenuFile();
    void initMenuPrint();
    void initMenuPlay();

    // Kör menyer
    void callMenuMain();
    void callMenuFile();
    void callMenuPrint();
    void callMenuPlay();

    // Filhantering
    void openFile();
    void saveFile();

    // Lägg till och ta bort
    void addAlbum();
    void deleteAlbum();

    // Utskrifter
    void printAlbum();
    void printAllSortName();
    void printAllSortLength();
    void printAllSimpleSortName();
    void printAllSimpleSortLength();

    // Spellista
    void createPlaylist();
    void playPlaylist();
    void shuffle();

public:
    Jukebox();
    void run();
};

// Hjälpfunktioner, ej medlemmar
bool cmpStringsInsensitive(std::string a1, std::string a2);
bool cmpAlbumName(const Album& album1, const Album& album2);
bool cmpAlbumLength(const Album& album1, const Album& album2);
void printSimpleName(const Album &album);
void printSimpleNameLength(const Album &album);
std::vector<int> extractInts(std::string input);
int randomInteger(int input);
bool inputTimeValid(const string &str);

#endif //DT019G_PROJECT_VIJO1803_JUKEBOX_H