if (POLICY CMP0048)
  cmake_policy(SET CMP0048 NEW)
endif (POLICY CMP0048)

#======================#
# Name of this project #
#======================#
set(this dt019g-project-vijo1803)

project(${this} VERSION 0.1 LANGUAGES CXX)

# Minimum required cmake version
cmake_minimum_required(VERSION 3.8)

# Compile with c++ 11 support
set(CMAKE_CXX_STANDARD 11)

#====================================#
# Source files needed for executable #
#====================================#
# Setting source files explicitly will allow cmake to detect
# the changes and reload the build script automatically.
# It also makes it easier to create multiple executables using
# different source files.
set(lib
    # Add any new source files here...
    src/Song.cpp
    include/Song.h
    src/Time.cpp
    include/Time.h
    src/Album.cpp
    include/Album.h
    src/Jukebox.cpp
    include/Jukebox.h
    src/Menu.cpp
    include/Menu.h
    src/MenuItem.cpp
    include/MenuItem.h
    src/Queue.cpp
    include/Queue.h
    include/GlobalConstants.h
)


#This should not need changing if you only create one executable.
set(exec
    ${lib}
    src/main.cpp
)

#=====================================#
# Files to include in the zip-archive #
#=====================================#
set(zip-files
    # If any files in here don't exist, creating the archive will fail.
    include
    src
    CMakeLists.txt
    laborationsbeskrivning.pdf
)

# Create executable, header files in include/ will be detected automatically (disabled by Victor).
add_executable(${PROJECT_NAME} ${exec})
# target_include_directories(${PROJECT_NAME} PRIVATE "./include")

#
# Create the zip file
#

# Name of zip file
set(archive_name "${CMAKE_CURRENT_SOURCE_DIR}/zip/${PROJECT_NAME}.zip")

# Command to create the archive.
# Will be called by any targets listing this OUTPUT as a DEPENDS
# The command will only run if any of the files listed in this
# DEPENDS are changed or if OUTPUT doesn't exist.
# Specifying the source directory as working directory means we avoid
# prefixing the files with "../".
add_custom_command(
    COMMAND ${CMAKE_COMMAND} -E tar "cfv" "${archive_name}" --format=zip
        -- ${zip-files}
    WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
    OUTPUT "${archive_name}"
    DEPENDS ${zip-files}
    COMMENT "Zipping source files to ${archive_name}."
)

# Triggering this target will call the custom command that OUTPUTs
# the file that this target DEPENDs on.
add_custom_target(zip DEPENDS "${archive_name}")
