/**
    @file MenuItem.cpp
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Implementation of the menu item class
*/

#include "../include/MenuItem.h"

using std::string;

MenuItem::MenuItem() = default;

MenuItem::MenuItem(string mt, bool en)
{
    menuText = mt;
    enabled = en;
}

bool MenuItem::isEnabled() const
{
    return enabled;
}

void MenuItem::enable()
{
    enabled = true;
}

void MenuItem::disable()
{
    enabled = false;
}

string MenuItem::getMenuText() const
{
    return menuText;
}