/**
    @file Time.cpp
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Implementation of the custom time class
*/

#include "../include/Time.h"
#include "../include/GlobalConstants.h"
#include <iostream>

using std::cout;
using std::string;
using std::istream;
using std::ostream;

Time::Time() = default;

Time::Time(int h, int m, int s)
{
    timmar = h;
    minuter = m;
    sekunder = s;
}

Time::Time(int s)
{
    timmar = s / 3600;
    int rest = s % 3600;
    minuter = rest / 60;
    rest = rest % 60;
    sekunder = rest;
}

int Time::getTimmar() const {return timmar;}
int Time::getMinuter() const {return minuter;}
int Time::getSekunder() const {return sekunder;}

void Time::setTimmar(const int t) {timmar = t;}
void Time::setMinuter(const int m) {minuter = m;}
void Time::setSekunder(const int s) {sekunder = s;}

int Time::toIntTime() const
{
    int intTime = sekunder + (minuter * 60) + (timmar * 3600);
    return intTime;
}

void Time::printElegant() const
{
    if (timmar > 0)
    {
        cout << timmar << ":";
        if (minuter < 10)
        {
            cout << "0" << minuter << ":";
        }
        else
        {
            cout << minuter << ":";
        }
        if (sekunder < 10)
        {
            cout << "0" << sekunder;
        }
        else
        {
            cout << sekunder;
        }
    }
    else
    {
        cout << minuter << ":";
        if (sekunder < 10)
        {
            cout << "0" << sekunder;
        }
        else
        {
            cout << sekunder;
        }
    }
}

Time Time::operator+(const Time &time) const
{
    int secTot = time.getSekunder() + (time.getMinuter() * 60) + (time.getTimmar() * 3600);
    secTot += getSekunder() + (getMinuter() * 60) + (getTimmar() * 3600);
    Time timeSum = Time(secTot);
    return timeSum;
}

bool Time::operator==(const Time &time) const
{
    return timmar == time.getTimmar() && minuter == time.getMinuter() && sekunder == time.getSekunder();
}

bool Time::operator<(const Time &time) const
{
    return (timmar*3600 + minuter*60 + sekunder) < (time.getTimmar()*3600 + time.getMinuter()*60 + time.getSekunder());
}




// ICKE-MEDLEMMAR NEDAN
ostream &operator<<(ostream &os, const Time &time)
{
    int intLength = time.getSekunder() + (time.getMinuter() * 60) + (time.getTimmar() * 3600);
    os << intLength << DELIM;
    return os;
}

istream &operator>>(istream &is, Time &time)
{
    string tempString;
    getline(is, tempString);
    int intLength;
    if (!tempString.empty())
    {
        intLength = stoi(tempString);
        // Konvertera till timeLength här
        int remainder;
        int hours = intLength / 3600;
        remainder = intLength % 3600;
        int minutes = remainder / 60;
        remainder = remainder % 60;
        int seconds = remainder;

        // Fixa Time-objektet
        time.setTimmar(hours);
        time.setMinuter(minutes);
        time.setSekunder(seconds);
    }
    return is;
}