/**
    @file Queue.cpp
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Implementation of the queue (playlist) class
*/

#include <thread>
#include <chrono>
#include "../include/Queue.h"
#include "../include/GlobalConstants.h"

using std::this_thread::sleep_for;
using std::cout;
using std::endl;

Queue::Queue()
{
    pointer = new Song[QUEUE_INCREMENT];
    size = QUEUE_INCREMENT;
    firstInQueue = 0;
    lastInQueue = 0;
}

Queue::~Queue()
{
    delete [] pointer;
    pointer = nullptr;
}

void Queue::enQueue(const Song &song)
{
    if (lastInQueue < size)
    {
        pointer[lastInQueue] = song;
    }
    else
    {
        increment();
        pointer[lastInQueue] = song;
    }
    lastInQueue++;
}

Song Queue::deQueue()
{
    if (firstInQueue <= lastInQueue)
    {
        Song deQueueSong = pointer[firstInQueue];
        firstInQueue++;
        return deQueueSong;
    }
}

void Queue::increment()
{
    Song *newPointer;
    newPointer = new Song[size + QUEUE_INCREMENT];
    for (int iter = 0; iter < size; iter++)
    {
        newPointer[iter] = pointer[iter];
    }
    delete [] pointer;
    size = size + QUEUE_INCREMENT;
    pointer = newPointer;
}

void Queue::play()
{
    while (firstInQueue < lastInQueue)
    {
        Song deQueued = deQueue();
        cout << "NOW PLAYING: ";
        deQueued.printSong();
        sleep_for(std::chrono::milliseconds(1500));
    }
}

// Impl. av överlagrad tilldelningsoperator
Queue& Queue::operator=(const Queue &Q)
{
    size = Q.size;
    firstInQueue = Q.firstInQueue;
    lastInQueue = Q.lastInQueue;

    pointer = new Song[size];
    for (int i = 0; i < size; i++)
    {
        pointer[i] = Q.pointer[i];
    }
}