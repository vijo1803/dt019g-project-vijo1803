/**
    @file main.cpp
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Containing the main function only
*/

#include "../include/Jukebox.h"

int main()
{
    Jukebox jukebox;
    jukebox.run();
}

// "ANVÄND ALGORITMER (FIND, FIND_IF) FÖR SÖKNING I LISTAN"