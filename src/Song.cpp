/**
    @file Song.cpp
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Implementation of the song class
*/

#include "../include/Song.h"
#include "../include/GlobalConstants.h"
#include "../include/Time.h"
#include <string>
#include <iostream>
#include <sstream>

using std::string;
using std::cout;
using std::endl;
using std::ostream;
using std::istream;
using std::istringstream;

Song::Song() = default;

/*
Song::Song(string &nTitle, string &nArtist, Time &nLength)
{
    title = nTitle;
    artist = nArtist;
    length = nLength;
} */

string Song::getTitle() const
{
    return title;
}

string Song::getArtist() const
{
    return artist;
}

Time Song::getLength() const
{
    return length;
}

void Song::setTitle(const string &nTitle)
{
    title = nTitle;
}

void Song::setArtist(const string &nArtist)
{
    artist = nArtist;
}

void Song::setLength(const Time &nLength)
{
    length = nLength;
}

void Song::printSong() const
{
    cout << artist << " - " << title << " (";
    length.printElegant();
    cout << ")" << endl;
}


// ICKE-MEDLEMMAR NEDAN

// Överlagring av <<-operatorn
ostream &operator<<(ostream &os, const Song &song)
{
    string title = song.getTitle();
    string artist = song.getArtist();
    Time length = song.getLength();

    os << title << DELIM;
    os << artist << DELIM;
    os << length;

    return os;
}

// Överlagring av >>-operatorn
istream &operator>>(istream &is, Song &song)
{
    string tempString;

    // Titel
    getline(is, tempString, DELIM);
    song.setTitle(tempString);

    // Artist
    getline(is, tempString, DELIM);
    song.setArtist(tempString);

    // Längd
    getline(is, tempString);
    if ((int)tempString.back() == 13) {tempString.pop_back();}
    istringstream iss;
    iss.str(tempString);
    Time tempLength;
    iss >> tempLength;
    song.setLength(tempLength);

    return is;
}