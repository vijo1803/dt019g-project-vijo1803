/**
    @file Menu.cpp
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Implementation of the menu class
*/

#include "../include/Menu.h"
#include <iostream>

using std::string;
using std::cout;
using std::endl;
using std::cin;

Menu::Menu() = default;

void Menu::addItem(string menuText, bool enabled)
{
    MenuItem menuItem = MenuItem(menuText, enabled);
    items.push_back(menuItem);
}

void Menu::printMenuItems() const
{
    cout << "=== " << menuTitle << " ===" << endl;
    int pos = 0;
    for (auto &e: items)
    {
        pos++;
        if (e.isEnabled())
        {
            cout << "(" << pos << ") ";
            cout << e.getMenuText() << endl;
        }
    }
    cout << "Your choice: ";
}

int Menu::getMenuChoice() const
{
    string input;
    getline(cin, input);
    bool formatError = false;
    for (auto e: input)
    {
        if (!isdigit(e)) {formatError = true;}
    }
    int choice = -1;

    // Om valet är ett heltal, spara det i choice
    if (!formatError) {choice = stoi(input);}

    // Om valet är större än antalet valmöjligheter i aktuell meny, sätt choice till -1
    if (choice > items.size())
    {
        choice = -1;
    }

        // Om valt alternativ inte är enabled, sätt choice till -1
    else if (!items[choice-1].isEnabled())
    {
        choice = -1;
    }

    return choice;
}

void Menu::setMenuTitle(string t)
{
    menuTitle = t;
}

void Menu::setAllEnabled()
{
    for (auto &e: items)
    {
        e.enable();
    }
}