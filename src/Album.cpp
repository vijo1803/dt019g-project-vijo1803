/**
    @file Album.cpp
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Implementation of the album class
*/


#include "../include/Album.h"
#include "../include/Song.h"
#include "../include/GlobalConstants.h"
#include <vector>
#include <iostream>
#include <string>

using std::vector;
using std::string;
using std::cout;
using std::endl;
using std::ostream;
using std::istream;

Album::Album() = default;

void Album::addSong(Song &s)
{
    songs.push_back(s);
}

string Album::getName() const
{
    return name;
}

vector<Song> Album::getSongs() const
{
    return songs;
}

void Album::setName(const string &n)
{
    name = n;
}

void Album::printAlbum() const
{
    cout << ALBUM_SYMBOL << " " << name << " " << ALBUM_SYMBOL << endl;
    for (auto &s: songs)
    {
        s.printSong();
    }
}

bool Album::operator<(const Album &album) const
{
    int thisLength = 0;
    int thatLength = 0;

    for (auto &e: songs)
    {
        thisLength += e.getLength().toIntTime();
    }

    for (auto &e: album.getSongs())
    {
        thatLength += e.getLength().toIntTime();
    }

    return thisLength < thatLength;
}

// ICKE-MEDLEMMAR NEDAN
ostream &operator<<(ostream &os, const Album &album)
{
    os << album.getName() << endl;
    os << album.getSongs().size() << endl;
    for (auto &e: album.getSongs())
    {
        os << e << endl;
    }
    return os;
}

istream &operator>>(istream &is, Album &album)
{
    string tempString;

    // Titel (om raden är tom, så görs inget)
    getline(is, tempString);
    if (!tempString.empty())
    {
        // Om sista tecknet är 13(hex) (radbrytningstecken) så tas detta bort
        if ((int)tempString.back() == 13) {tempString.pop_back();}
        album.setName(tempString);

        // Antal låtar
        getline(is, tempString);
        int noOfSongs = stoi(tempString);

        // Låtarna
        int iter = 0;
        Song tempSong;
        while (iter < noOfSongs)
        {
            is >> tempSong;
            album.addSong(tempSong);
            iter++;
        }
    }
}