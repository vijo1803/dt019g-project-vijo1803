/**
    @file Jukebox.cpp
    @author Victor Johansson (vijo1803)
    @date March 2019
    @version: 0.1
    @brief Implementation of the jukebox (user interface) class
*/

#include "../include/Jukebox.h"
#include "../include/GlobalConstants.h"
#include "../include/Time.h"
#include <string>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <random>

using std::cout;
using std::endl;
using std::ios;
using std::vector;
using std::fstream;
using std::string;
using std::cin;
using std::stringstream;
using std::random_device;
using std::uniform_int_distribution;
using std::mt19937;
using std::reverse;

Jukebox::Jukebox()
{
    // Initiera menyerna
    initMenuMain();
    initMenuFile();
    initMenuPrint();
    initMenuPlay();
}

void Jukebox::run()
{
    callMenuMain();
}

void Jukebox::initMenuMain()
{
    menuMain.setMenuTitle("MAIN MENU");
    menuMain.addItem("File...", true);
    menuMain.addItem("Add an album", false);
    menuMain.addItem("Delete an album", false);
    menuMain.addItem("Print...", false);
    menuMain.addItem("Play...", false);
    menuMain.addItem("Exit", true);
}

void Jukebox::initMenuFile()
{
    menuFile.setMenuTitle("FILE MENU");
    menuFile.addItem("Open", true);
    menuFile.addItem("Save", false);
    menuFile.addItem("Back to main menu...", true);
}

void Jukebox::initMenuPrint()
{
    menuPrint.setMenuTitle("PRINT MENU");
    menuPrint.addItem("Print album", true);
    menuPrint.addItem("Print all (sort by album name)", true);
    menuPrint.addItem("Print all (sort by length)", true);
    menuPrint.addItem("Print all (name only) (sort by album name)", true);
    menuPrint.addItem("Print all (name and length only) (sort by length)", true);
    menuPrint.addItem("Back to main menu...", true);
}

void Jukebox::initMenuPlay()
{
    menuPlay.setMenuTitle("PLAY MENU");
    menuPlay.addItem("Create playlist", true);
    menuPlay.addItem("Create shuffle playlist", true);
    menuPlay.addItem("Play playlist", true);
    menuPlay.addItem("Back to main menu...", true);
}

void Jukebox::callMenuMain()
{
    bool again = true;
    while (again)
    {
        menuMain.printMenuItems();
        int choice = menuMain.getMenuChoice();
        switch (choice)
        {
            case 1: callMenuFile(); break;
            case 2: addAlbum(); break;
            case 3: deleteAlbum(); break;
            case 4: callMenuPrint(); break;
            case 5: callMenuPlay(); break;
            case 6: again = false; break;
            default: break;
        }
    }
}

void Jukebox::callMenuFile()
{
    bool again = true;
    while (again)
    {
        menuFile.printMenuItems();
        int choice = menuFile.getMenuChoice();
        switch (choice)
        {
            case 1: openFile(); break;
            case 2: saveFile(); break;
            case 3: again = false; break;
            default: break;
        }
    }
}

void Jukebox::callMenuPrint()
{
    bool again = true;
    while (again)
    {
        menuPrint.printMenuItems();
        int choice = menuPrint.getMenuChoice();
        switch (choice)
        {
            case 1: printAlbum(); break;
            case 2: printAllSortName(); break;
            case 3: printAllSortLength(); break;
            case 4: printAllSimpleSortName(); break;
            case 5: printAllSimpleSortLength(); break;
            case 6: again = false; break;
            default: break;
        }
    }
}

void Jukebox::callMenuPlay()
{
    bool again = true;
    while (again)
    {
        menuPlay.printMenuItems();
        int choice = menuPlay.getMenuChoice();
        switch (choice)
        {
            case 1: createPlaylist(); break;
            case 2: shuffle(); break;
            case 3: playPlaylist(); break;
            case 4: again = false; break;
            default: break;
        }
    }
}

void Jukebox::openFile()
{
    vector<Album> loadedAlbumList;
    fstream inFile(FILE_NAME, ios::in);
    if (!inFile.is_open())
    {
        cout << "Unable to open file." << endl;
    }
    else
    {
        while (!inFile.eof())
        {
            Album tempAlbum;
            inFile >> tempAlbum;
            loadedAlbumList.push_back(tempAlbum);
        }
    }
    inFile.close();

    // Om sista albumet som laddades in saknar låtar, ta bort det
    size_t lastLoadedPos = loadedAlbumList.size() - 1;
    if (loadedAlbumList[lastLoadedPos].getSongs().empty() && loadedAlbumList[lastLoadedPos].getName().empty())
    {
        loadedAlbumList.pop_back();
    }
    albums = loadedAlbumList;

    // Om album lästs in, gör alla val möjliga
    if (!albums.empty())
    {
        menuMain.setAllEnabled();
        menuFile.setAllEnabled();
    }
}

void Jukebox::saveFile()
{
    fstream outFile(FILE_NAME, ios::out);
    for (auto &a: albums)
    {
        if (!a.getName().empty())
        {
            outFile << a;
        }
    }
    outFile.close();
}

void Jukebox::addAlbum()
{
    Album inputAlbum;
    cout << "Enter album title: ";
    string inputName;
    getline(cin, inputName);
    bool unique = true;
    if (!inputName.empty())
    {
        for (auto &a: albums)
        {
            if (cmpStringsInsensitive(a.getName(), inputName))
            {
                unique = false;
            }
        }
        inputAlbum.setName(inputName);
    }
    if (unique)
    {
        cout << "Enter number of tracks: ";
        string noOfTracks;
        getline(cin, noOfTracks);
        bool formatError = false;
        for (auto &c: noOfTracks)
        {
            if (!isdigit(c))
            {
                formatError = true;
            }
        }
        int intNoOfTracks = 0;
        if (!formatError)
        {
            intNoOfTracks = stoi(noOfTracks);
            if (intNoOfTracks < 1 || intNoOfTracks > MAX_NO_OF_TRACKS_ON_ALBUM)
            {
                formatError = true;
            }
        }
        if (formatError)
        {
            cout << "Aborted. Please enter a number between 1 and " << MAX_NO_OF_TRACKS_ON_ALBUM << "." << endl;
        }
        else
        {
            int i = 0;
            while (i < intNoOfTracks)
            {
                Song song;
                cout << "(Track " << i+1 << ") Artist/group: ";
                string inputArtist;
                getline(cin, inputArtist);
                song.setArtist(inputArtist);
                cout << "(Track " << i+1 << ") Title: ";
                string inputTitle;
                getline(cin, inputTitle);
                song.setTitle(inputTitle);
                cout << "(Track " << i+1 << ") Length, [hh:mm:ss] (example: 00:02:20): ";
                string inputLength;
                getline(cin, inputLength);
                if (inputTimeValid(inputLength))
                {
                    string hs = inputLength.substr(0, inputLength.find(':'));
                    inputLength.erase(0, inputLength.find(':') + 1);
                    string ms = inputLength.substr(0, inputLength.find(':'));
                    inputLength.erase(0, inputLength.find(':') + 1);
                    string ss = inputLength;
                    int h = stoi(hs);
                    int m = stoi(ms);
                    int s = stoi(ss);
                    if (h >= 0 && m >= 0 && m < 60 && s >= 0 && s < 60)
                    {
                        Time inputTime = Time(h, m, s);
                        song.setLength(inputTime);
                        inputAlbum.addSong(song);
                    }
                    else
                    {
                        cout << "Wrong input time format. Enter song details again." << endl;
                        i--;
                    }
                    i++;
                }
                else
                {
                    cout << "Wrong input time format. Enter song details again." << endl;
                }
            }
        }
        albums.push_back(inputAlbum);
    }
    else
    {
        cout << "Album name is not unique." << endl;
    }
}

void Jukebox::deleteAlbum()
{
    cout << "Enter album name: ";
    string inputAlbum;
    getline(cin, inputAlbum);
    int found = -1;
    int i = 0;
    for (auto &a: albums)
    {
        if (cmpStringsInsensitive(inputAlbum, a.getName()))
        {
            found = i;
        }
        i++;
    }
    if (found == -1)
    {
        cout << "Album could not be found." << endl;
    }
    else
    {
        albums.erase(albums.begin() + found);
        cout << "Album deleted." << endl;
    }
}


// UTSKRIFTSFUNKTIONER NEDAN
void Jukebox::printAlbum()
{
    cout << "Enter album name: ";
    string inputAlbum;
    getline(cin, inputAlbum);
    bool found = false;
    for (auto &a: albums)
    {
        if (cmpStringsInsensitive(inputAlbum, a.getName()))
        {
            a.printAlbum();
            found = true;
        }
    }
    if (!found)
    {
        cout << "Album could not be found." << endl;
    }
}

void Jukebox::printAllSortName()
{
    vector<Album> albumsCopy = albums;
    sort(albumsCopy.begin(), albumsCopy.end(), cmpAlbumName);
    for (auto &a: albumsCopy)
    {
        a.printAlbum();
        cout << SECTION_DELIM << endl;
    }
}

void Jukebox::printAllSortLength()
{
    vector<Album> albumsCopy = albums;
    sort(albumsCopy.begin(), albumsCopy.end(), cmpAlbumLength);
    reverse(albumsCopy.begin(), albumsCopy.end());
    for (auto &a: albumsCopy)
    {
        a.printAlbum();
        cout << SECTION_DELIM << endl;
    }
}

void Jukebox::printAllSimpleSortName()
{
    vector<Album> albumsCopy = albums;
    sort(albumsCopy.begin(), albumsCopy.end(), cmpAlbumName);
    for_each(albumsCopy.begin(), albumsCopy.end(), printSimpleName);
}

void Jukebox::printAllSimpleSortLength()
{
    vector<Album> albumsCopy = albums;
    sort(albumsCopy.begin(), albumsCopy.end(), cmpAlbumLength);
    reverse(albumsCopy.begin(), albumsCopy.end());
    for_each(albumsCopy.begin(), albumsCopy.end(), printSimpleNameLength);
}

//PLAYLIST-FUNKTIONER
void Jukebox::createPlaylist()
{
    Queue newQueue;
    vector<Song> allSongs;
    for (auto &a: albums)
    {
        vector<Song> albumSongs = a.getSongs();
        for (auto &s: albumSongs)
        {
            allSongs.push_back(s);
        }
    }
    int i = 1;
    for (auto &s: allSongs)
    {
        cout << i << ": ";
        s.printSong();
        i++;
    }

    cout << "Songs to add to playlist (comma-sep., example: 3, 17, 2): ";
    string playlistInput;
    getline(cin, playlistInput);
    vector<int> songNumbers = extractInts(playlistInput);
    for (auto &nr: songNumbers)
    {
            newQueue.enQueue(allSongs[nr-1]);
    }
    queue = newQueue;
}

void Jukebox::playPlaylist()
{
    Queue copy;
    copy = queue;
    copy.play();
}

void Jukebox::shuffle()
{
    // Samla sånger i en vektor
    vector<Song> allSongs;
    for (auto &a: albums)
    {
        vector<Song> albumSongs = a.getSongs();
        for (auto &s: albumSongs)
        {
            allSongs.push_back(s);
        }
    }

    // Dialog
    cout << "Number of songs to add to shuffle playlist: ";
    string inputNumberString;
    getline(cin, inputNumberString);
    int inputNumber = stoi(inputNumberString);

    // Spara historik, för att kontrollera för dubbletter
    vector<int> songsIncluded;

    // Det finns begränsat med unika sånger
    if (inputNumber > allSongs.size())
    {
        cout << "There are not that many unique songs." << endl;
    }
    else
    {
        Queue newQueue; // Kö att ersätta den gamla kön
        for (int i = 0; i < inputNumber; i++)
        {
            int randomized = randomInteger(allSongs.size()-1);
            bool exists = false;

            for (auto &nr: songsIncluded)
            {
                if (nr == randomized)
                {
                    exists = true;
                }
            }

            if (!exists)
            {
                songsIncluded.push_back(randomized);
                newQueue.enQueue(allSongs[randomized]);
            }
            else
            {
                i--;
            }
        }
        queue = newQueue;
    }
}


// ICKE-MEDLEMMAR (HJÄLPFUNKTIONER)
bool cmpStringsInsensitive(const string a1, const string a2)
{
    string aInsensitive;
    string bInsensitive;
    for (auto c: a1)
    {
        aInsensitive += (char)toupper(c);
    }
    for (auto c: a2)
    {
        bInsensitive += (char)toupper(c);
    }
    return aInsensitive == bInsensitive;
}

bool cmpAlbumName(const Album& album1, const Album& album2)
{
    bool cmp = false;

    // Gör allt till uppercase innan jämförelse
    string a1 = album1.getName();
    for (auto c: a1)
    {
        c = (char)toupper(c);
    }
    string a2 = album2.getName();
    for (auto c: a2)
    {
        c = (char)toupper(c);
    }

    if (a1 < a2) {cmp = true;}
    return cmp;
}

bool cmpAlbumLength(const Album& album1, const Album& album2)
{
    // Överlagring av operatorn "<" används här
    bool cmp = false;
    if (album1 < album2) {cmp = true;}
    return cmp;
}

void printSimpleName(const Album &album)
{
    cout << album.getName() << endl;
}

void printSimpleNameLength(const Album &album)
{
    vector<Song> songs = album.getSongs();
    Time totalTime = Time(0);
    for (auto &s: songs)
    {
        // Använd Time-klassens operatoröverlagring
        totalTime = totalTime + s.getLength();
    }
    cout << album.getName() << " (";
    totalTime.printElegant();
    cout << ")" << endl;
}

// SMIDIG LÖSNING (insp: https://stackoverflow.com/questions/1894886/parsing-a-comma-delimited-stdstring)
vector<int> extractInts(const string input)
{
    vector<int> songNumbers;
    stringstream ss(input);
    int i;
    while (ss >> i)
    {
        if (ss.peek() == ',' || ss.peek() == ' ')
        {
            ss.ignore();
        }
        songNumbers.push_back(i);
    }
    return songNumbers;
}

int randomInteger(const int input)
{
    random_device rd;
    mt19937 eng(rd());
    uniform_int_distribution<> distr(0, input);
    int generated = distr(eng);
    return generated;
}

bool inputTimeValid(const string &str)
{
    bool formatError = false;
    int delims = 0;
    for (auto &c: str)
    {
        if (!isdigit(c) && c != ':')
        {
            formatError = true;
        }
        else if (c == ':')
        {
            delims++;
        }
    }
    return !formatError && delims == 2 && str.length() == 8;
}